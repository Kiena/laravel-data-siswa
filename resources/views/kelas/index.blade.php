@extends('layout.index')
@section('title')
    Dashboard Kelas
@endsection

@section('content')

<div class="grid gap-6 mb-8 md:grid-cols-2 xl:grid-cols-4">
    <!-- Card -->
    <div
      class="flex items-center p-4 bg-white rounded-lg shadow-xs dark:bg-gray-800"
    >
      <div
        class="p-3 mr-4 text-green-500 bg-green-100 rounded-full dark:text-green-100 dark:bg-green-500"
      >
      <svg class="w-6 h-6" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path d="M12 14l9-5-9-5-9 5 9 5z"></path><path d="M12 14l6.16-3.422a12.083 12.083 0 01.665 6.479A11.952 11.952 0 0012 20.055a11.952 11.952 0 00-6.824-2.998 12.078 12.078 0 01.665-6.479L12 14z"></path><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M12 14l9-5-9-5-9 5 9 5zm0 0l6.16-3.422a12.083 12.083 0 01.665 6.479A11.952 11.952 0 0012 20.055a11.952 11.952 0 00-6.824-2.998 12.078 12.078 0 01.665-6.479L12 14zm-4 6v-7.5l4-2.222"></path></svg>
      </div>
      <div>
        <p
          class="mb-2 text-sm font-medium text-gray-600 dark:text-gray-400"
        >
          Total Kelas
        </p>
        <p
          class="text-lg font-semibold text-gray-700 dark:text-gray-200"
        >
            {{$kelas->count()}}
        </p>
      </div>
    </div>
</div>
@auth
<div class="my-6">
    <a href="/kelas/create" class="btn items-center justify-between px-4 py-2 text-sm text-white transition-colors duration-150 bg-purple-600 border border-transparent rounded-lg active:bg-purple-600 hover:bg-purple-700 focus:outline-none focus:shadow-outline-purple"
    >
    Tambah
    <span class="ml-2" aria-hidden="true">+</span>
    </a>
</div>
@endauth
<div class="w-full overflow-hidden rounded-lg shadow-xs">
    <div class="w-full overflow-x-auto">
        <table class="w-full whitespace-no-wrap">
        <thead>
            <tr
            class="text-xs font-semibold tracking-wide text-left text-gray-500 uppercase border-b dark:border-gray-700 bg-gray-50 dark:text-gray-400 dark:bg-gray-800"
            >
            <th class="px-4 py-3">No</th>
            <th class="px-4 py-3">Kelas</th>
            <th class="px-4 py-3">Terakhir diubah</th>
            <th class="px-4 py-3">Tindakan</th>
            </tr>
        </thead>
        <tbody class="bg-white divide-y dark:divide-gray-700 dark:bg-gray-800">
        @forelse ($kelas as $key=>$value)
            <tr class="text-gray-700 dark:text-gray-400">
            <td class="px-4 py-3">
                <div class="flex items-center text-sm">
                <!-- Avatar with inset shadow -->
                <div>
                    <p class="font-semibold">{{$key + 1}}</p>
                    </p>
                </div>
                </div>
            </td>
            <td class="px-4 py-3 text-sm">
                {{$value->kelas}}
            </td>
            </td>
            <td class="px-4 py-3 text-sm">
                {{$value->created_at}}
            </td>
            <td class="px-4 py-3">
                <form action="/kelas/{{$value->id}}" method="POST">
                    @csrf
                    <div class="flex items-center space-x-4 text-sm">
                    @method('DELETE')
                      @auth
                      <a href="/kelas/{{$value->id}}/edit"
                        class="flex items-center justify-between px-2 py-2 text-sm font-medium leading-5 text-purple-600 rounded-lg dark:text-gray-400 focus:outline-none focus:shadow-outline-gray"
                        aria-label="Edit"
                      >
                        <svg
                          class="w-5 h-5"
                          aria-hidden="true"
                          fill="currentColor"
                          viewBox="0 0 20 20"
                        >
                          <path
                            d="M13.586 3.586a2 2 0 112.828 2.828l-.793.793-2.828-2.828.793-.793zM11.379 5.793L3 14.172V17h2.828l8.38-8.379-2.83-2.828z"
                          ></path>
                        </svg>
                        </a>
                      <button type="submit" onclick="return confirm('Apakah anda yakin?')" value="Delete"
                        class="flex items-center justify-between px-2 py-2 text-sm font-medium leading-5 text-purple-600 rounded-lg dark:text-gray-400 focus:outline-none focus:shadow-outline-gray"
                        
                      >
                        <svg
                          class="w-5 h-5"
                          aria-hidden="true"
                          fill="currentColor"
                          viewBox="0 0 20 20"
                        >
                          <path
                            fill-rule="evenodd"
                            d="M9 2a1 1 0 00-.894.553L7.382 4H4a1 1 0 000 2v10a2 2 0 002 2h8a2 2 0 002-2V6a1 1 0 100-2h-3.382l-.724-1.447A1 1 0 0011 2H9zM7 8a1 1 0 012 0v6a1 1 0 11-2 0V8zm5-1a1 1 0 00-1 1v6a1 1 0 102 0V8a1 1 0 00-1-1z"
                            clip-rule="evenodd"
                          ></path>
                        </svg>
                      </button>
                      @endauth
                    </div>
                </form>
                </td>

            </tr>


            @empty
            <tr class="text-gray-700 dark:text-gray-400" colspan="3">
            <td class="px-4 py-3">
                <p class="text-sm font-semibold">No data</p>
            </td>
            </tr>  
        @endforelse

        </tbody>
        </table>
    </div>
</div>
@endsection


