@extends('layout.index')
@section('title')
    Edit Kelas {{$kelas->kelas}}
@endsection

@section('content')
    <form action="/kelas/{{$kelas->id}}" method="POST" enctype="multipart/form-data">
        @csrf
        @method('PUT')

        <!-- Input form -->
        <div class="px-4 py-3 mb-8 bg-white rounded-lg shadow-md dark:bg-gray-800">

            <label class="block text-sm">
            <span class="text-gray-700 dark:text-gray-400">Kelas</span>
            
            <div
                class="relative text-gray-500 focus-within:text-purple-600 dark:focus-within:text-purple-400">
                <input class="block w-full pl-10 mt-1 text-sm text-black dark:text-gray-300 dark:border-gray-600 dark:bg-gray-700 focus:border-purple-400 focus:outline-none focus:shadow-outline-purple dark:focus:shadow-outline-gray form-input" value="{{$kelas->kelas}}" placeholder="Masukkan kelas" name="kelas"/>
                @error('kelas')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
                @enderror
                <div class="absolute inset-y-0 flex items-center ml-3 pointer-events-none">
                    <svg class="w-4 h-4" aria-hidden="true" fill="none" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" viewBox="0 0 24 24" stroke="currentColor" xmlns="http://www.w3.org/2000/svg">
                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M16 7a4 4 0 11-8 0 4 4 0 018 0zM12 14a7 7 0 00-7 7h14a7 7 0 00-7-7z">
                        </path>
                    </svg>
                </div>
            </div>
            </label>
        </div>
        <!-- End input form -->
        
       @auth
        <div class="my-6">
            <button class="btn items-center justify-between px-4 py-2 text-sm text-white transition-colors duration-150 bg-purple-600 border border-transparent rounded-lg active:bg-purple-600 hover:bg-purple-700 focus:outline-none focus:shadow-outline-purple" type="submit">
            Update
                <!-- <span class="ml-2" aria-hidden="true">+</span> -->
            </button>
        </div>
        @endauth
    </form>
@endsection