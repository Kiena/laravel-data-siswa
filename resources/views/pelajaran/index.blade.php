@extends('layout.index')
@section('title')
    Dashboard Mapel
@endsection

@section('content')
<div class="grid gap-6 mb-8 md:grid-cols-2 xl:grid-cols-4">
    <!-- Card -->
    <div
      class="flex items-center p-4 bg-white rounded-lg shadow-xs dark:bg-gray-800"
    >
      <div
        class="p-3 mr-4 text-teal-500 bg-teal-100 rounded-full dark:text-teal-100 dark:bg-teal-500"
      >
        <svg class="w-6 h-6" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M12 6.253v13m0-13C10.832 5.477 9.246 5 7.5 5S4.168 5.477 3 6.253v13C4.168 18.477 5.754 18 7.5 18s3.332.477 4.5 1.253m0-13C13.168 5.477 14.754 5 16.5 5c1.747 0 3.332.477 4.5 1.253v13C19.832 18.477 18.247 18 16.5 18c-1.746 0-3.332.477-4.5 1.253"></path></svg>
      </div>
      <div>
        <p
          class="mb-2 text-sm font-medium text-gray-600 dark:text-gray-400"
        >
          Total Mapel
        </p>
        <p
          class="text-lg font-semibold text-gray-700 dark:text-gray-200"
        >
            {{$pelajaran->count()}}
        </p>
      </div>
    </div>
</div>
@auth
<div class="my-6">
    <a href="/pelajaran/create" class="btn items-center justify-between px-4 py-2 text-sm text-white transition-colors duration-150 bg-purple-600 border border-transparent rounded-lg active:bg-purple-600 hover:bg-purple-700 focus:outline-none focus:shadow-outline-purple"
    >
    Tambah
    <span class="ml-2" aria-hidden="true">+</span>
    </a>
</div>
@endauth
<div class="w-full overflow-hidden rounded-lg shadow-xs">
    <div class="w-full overflow-x-auto">
        <table class="w-full whitespace-no-wrap">
        <thead>
            <tr
            class="text-xs font-semibold tracking-wide text-left text-gray-500 uppercase border-b dark:border-gray-700 bg-gray-50 dark:text-gray-400 dark:bg-gray-800"
            >
            <th class="px-4 py-3">No</th>
            <th class="px-4 py-3">Mapel</th>
            <th class="px-4 py-3">Guru</th>
            <th class="px-4 py-3">Terakhir Diubah</th>
            <th class="px-4 py-3">Tindakan</th>
            </tr>
        </thead>
        <tbody class="bg-white divide-y dark:divide-gray-700 dark:bg-gray-800">
        @forelse ($pelajaran as $key=>$value)
            <tr class="text-gray-700 dark:text-gray-400">
            <td class="px-4 py-3">
                <div class="flex items-center text-sm">
                <!-- Avatar with inset shadow -->
                <div>
                    <p class="font-semibold">{{$key + 1}}</p>
                    </p>
                </div>
                </div>
            </td>
            <td class="px-4 py-3 text-sm">
                {{$value->mapel}}
            </td>
            <td class="px-4 py-3 text-sm">
                {{$value->guru->nama}}
            </td>
            <td class="px-4 py-3 text-sm">
                {{$value->updated_at}}
            </td>

            <td class="px-4 py-3">
                <form action="/pelajaran/{{$value->id}}" method="POST">
                    @csrf
                    <div class="flex items-center space-x-4 text-sm">
                    @method('DELETE')
                      @auth
                      <a href="/pelajaran/{{$value->id}}/edit"
                        class="flex items-center justify-between px-2 py-2 text-sm font-medium leading-5 text-purple-600 rounded-lg dark:text-gray-400 focus:outline-none focus:shadow-outline-gray"
                        aria-label="Edit"
                      >
                        <svg
                          class="w-5 h-5"
                          aria-hidden="true"
                          fill="currentColor"
                          viewBox="0 0 20 20"
                        >
                          <path
                            d="M13.586 3.586a2 2 0 112.828 2.828l-.793.793-2.828-2.828.793-.793zM11.379 5.793L3 14.172V17h2.828l8.38-8.379-2.83-2.828z"
                          ></path>
                        </svg>
                        </a>
                      <button type="submit" onclick="return confirm('Apakah anda yakin?')" value="Delete"
                        class="flex items-center justify-between px-2 py-2 text-sm font-medium leading-5 text-purple-600 rounded-lg dark:text-gray-400 focus:outline-none focus:shadow-outline-gray"
                        
                      >
                        <svg
                          class="w-5 h-5"
                          aria-hidden="true"
                          fill="currentColor"
                          viewBox="0 0 20 20"
                        >
                          <path
                            fill-rule="evenodd"
                            d="M9 2a1 1 0 00-.894.553L7.382 4H4a1 1 0 000 2v10a2 2 0 002 2h8a2 2 0 002-2V6a1 1 0 100-2h-3.382l-.724-1.447A1 1 0 0011 2H9zM7 8a1 1 0 012 0v6a1 1 0 11-2 0V8zm5-1a1 1 0 00-1 1v6a1 1 0 102 0V8a1 1 0 00-1-1z"
                            clip-rule="evenodd"
                          ></path>
                        </svg>
                      </button>
                      @endauth
                    </div>
                </form>
                </td>

            </tr>
            @empty
            <tr class="text-gray-700 dark:text-gray-400" colspan="3">
            <td class="px-4 py-3">
                <p class="text-sm font-semibold">No data</p>
            </td>
            
            

            </tr>  
        @endforelse

        </tbody>
        </table>
    </div>
</div>
@endsection


