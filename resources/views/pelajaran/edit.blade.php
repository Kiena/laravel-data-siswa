@extends('layout.index')
@section('title')
    Edit Mapel {{$pelajaran->mapel}}
@endsection

@section('content')
    <form action="/pelajaran/{{$pelajaran->id}}" method="POST" enctype="multipart/form-data">
        @csrf
        @method('PUT')

        <!-- Input form -->
        <div class="px-4 py-3 mb-8 bg-white rounded-lg shadow-md dark:bg-gray-800">

            <label class="block text-sm">
            <span class="text-gray-700 dark:text-gray-400">pelajaran</span>
            
            <div
                class="relative text-gray-500 focus-within:text-purple-600 dark:focus-within:text-purple-400">
                <input class="block w-full pl-10 mt-1 text-sm text-black dark:text-gray-300 dark:border-gray-600 dark:bg-gray-700 focus:border-purple-400 focus:outline-none focus:shadow-outline-purple dark:focus:shadow-outline-gray form-input" value="{{$pelajaran->mapel}}" placeholder="Ubah Mapel" name="mapel"/>
                @error('mapel')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
                @enderror
                <div class="absolute inset-y-0 flex items-center ml-3 pointer-events-none">
                  <svg class="w-4 h-4" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M12 6.253v13m0-13C10.832 5.477 9.246 5 7.5 5S4.168 5.477 3 6.253v13C4.168 18.477 5.754 18 7.5 18s3.332.477 4.5 1.253m0-13C13.168 5.477 14.754 5 16.5 5c1.747 0 3.332.477 4.5 1.253v13C19.832 18.477 18.247 18 16.5 18c-1.746 0-3.332.477-4.5 1.253"></path></svg>
                </div>
            </div>
            </label>

            <label class="block mt-4 text-sm">
                <span class="text-gray-700 dark:text-gray-400">Guru</span>
                  <select name="guru_id" class="block w-full mt-1 text-sm dark:text-gray-300 dark:border-gray-600 dark:bg-gray-700 form-select focus:border-purple-400 focus:outline-none focus:shadow-outline-purple dark:focus:shadow-outline-gray">
                    @foreach ($guru as $item)
                      @if ($item->id === $pelajaran->guru_id)
                      <option value="{{$item->id}}" selected>{{$item->nama}}</option>
                      @else
                      <option value="{{$item->id}}">{{$item->nama}}</option>
                      @endif
                    @endforeach
                  </select>
                @error('guru_id')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
                @enderror
              </label>
        </div>
        <!-- End input form -->

        
       @auth
        <div class="my-6">
            <button class="btn items-center justify-between px-4 py-2 text-sm text-white transition-colors duration-150 bg-purple-600 border border-transparent rounded-lg active:bg-purple-600 hover:bg-purple-700 focus:outline-none focus:shadow-outline-purple" type="submit">
            Update
                <!-- <span class="ml-2" aria-hidden="true">+</span> -->
            </button>
        </div>
        @endauth
    </form>
@endsection