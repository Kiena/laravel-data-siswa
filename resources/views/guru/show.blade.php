@extends('layout.index')
@section('title')
    Data Guru
@endsection

@section('content')

<!-- Cards with title -->

<div class="grid justify-center gap-6 mb-8 md:grid-cols-2">
    <!-- Profile-->
    <div
    class="min-w-0 p-4 bg-white rounded-lg shadow-xs dark:bg-gray-800"
    >
    <h4 class="mb-4 font-semibold text-gray-800 dark:text-gray-300">
        Profile
    </h4>
    <img src="{{asset('images/'.$guru->gambar)}}" class="rounded-full mx-auto mt-8 mb-8" alt="..." width="200" height="200">
        <div
            class="flex justify-center mt-4 space-x-3 text-sm text-gray-600 dark:text-gray-400"
        >
            <!-- Chart legend -->

            <div class="flex items-center">
            <span class="px-2 py-1 font-semibold leading-tight text-green-700 bg-green-100 rounded-full dark:bg-green-700 dark:text-green-100">
            {{$guru->nama}}
            </span>
            
            </div>
            <div class="flex items-center">
            <span class="px-2 py-1 font-semibold leading-tight text-green-700 bg-green-100 rounded-full dark:bg-green-700 dark:text-green-100">
            {{$guru->jabatan}}
            </span>
            </div>

        </div>
        <p class="text-center text-gray-600 mt-8 dark:text-gray-400">
            Lorem ipsum dolor sit, amet consectetur adipisicing elit.
            Fuga, cum commodi a omnis numquam quod? Totam exercitationem
            quos hic ipsam at qui cum numquam, sed amet ratione! Ratione,
            nihil dolorum.
        </p>
    </div>
    
    <!-- form -->
    <div class="min-w-0 p-4 bg-white rounded-lg shadow-xs dark:bg-gray-800">
        <h4 class="mb-4 font-semibold text-gray-800 dark:text-gray-300">
            Data Diri
        </h4>
            <label class="block text-sm">
            <span class="text-gray-700 dark:text-gray-400">Nama</span>
            
            <div
                class="relative text-gray-500 focus-within:text-purple-600 dark:focus-within:text-purple-400">
                <input class="block w-full pl-10 mt-1 text-sm text-black dark:text-gray-300 dark:border-gray-600 dark:bg-gray-700 focus:border-purple-400 focus:outline-none focus:shadow-outline-purple dark:focus:shadow-outline-gray form-input" value="{{$guru->nama}}" placeholder="Masukkan Nama" name="nama" disabled/>
                <div class="absolute inset-y-0 flex items-center ml-3 pointer-events-none">
                    <svg class="w-4 h-4" aria-hidden="true" fill="none" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" viewBox="0 0 24 24" stroke="currentColor" xmlns="http://www.w3.org/2000/svg">
                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M16 7a4 4 0 11-8 0 4 4 0 018 0zM12 14a7 7 0 00-7 7h14a7 7 0 00-7-7z">
                        </path>
                    </svg>
                </div>
            </div>
            </label>

            <label class="block mt-4 text-sm">
            <span class="text-gray-700 dark:text-gray-400">NIP</span>
            
            <div
                class="relative text-gray-500 focus-within:text-purple-600 dark:focus-within:text-purple-400">
                <input class="block w-full pl-10 mt-1 text-sm text-black dark:text-gray-300 dark:border-gray-600 dark:bg-gray-700 focus:border-purple-400 focus:outline-none focus:shadow-outline-purple dark:focus:shadow-outline-gray form-input" value="{{$guru->nip}}" placeholder="Masukkan NIP" name="nip" disabled/>
                <div class="absolute inset-y-0 flex items-center ml-3 pointer-events-none">
                    <svg class="w-4 h-4" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M14 10l-2 1m0 0l-2-1m2 1v2.5M20 7l-2 1m2-1l-2-1m2 1v2.5M14 4l-2-1-2 1M4 7l2-1M4 7l2 1M4 7v2.5M12 21l-2-1m2 1l2-1m-2 1v-2.5M6 18l-2-1v-2.5M18 18l2-1v-2.5"></path>
                    </svg>
                </div>
            </div>
            </label>

            <label class="block mt-4 text-sm">
            <span class="text-gray-700 dark:text-gray-400">Jabatan</span>
            
            <div
                class="relative text-gray-500 focus-within:text-purple-600 dark:focus-within:text-purple-400">
                <input class="block w-full pl-10 mt-1 text-sm text-black dark:text-gray-300 dark:border-gray-600 dark:bg-gray-700 focus:border-purple-400 focus:outline-none focus:shadow-outline-purple dark:focus:shadow-outline-gray form-input" value="{{$guru->jabatan}}" placeholder="Masukkan Jabatan" name="jabatan" disabled/>
                <div class="absolute inset-y-0 flex items-center ml-3 pointer-events-none">
                <svg class="w-4 h-4" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M8 7h12m0 0l-4-4m4 4l-4 4m0 6H4m0 0l4 4m-4-4l4-4"></path>
                </svg>
                </div>
            </div>
            </label>
            @auth
            <div class="my-6">
            <a href="/guru/{{$guru->id}}/edit" class="btn items-center justify-between px-4 py-2 text-sm text-white transition-colors duration-150 bg-purple-600 border border-transparent rounded-lg active:bg-purple-600 hover:bg-purple-700 focus:outline-none focus:shadow-outline-purple" type="submit">
            Edit Data
                <!-- <span class="ml-2" aria-hidden="true">+</span> -->
            </a>
        </div>
        @endauth
        
    </div>

</div>


@endsection