@extends('layout.index')
@section('title')
    Buat Data Guru
@endsection
@section('content')
    <form action="/guru" method="POST" enctype="multipart/form-data">
        @csrf

        <!-- Input form -->
        <div class="px-4 py-3 mb-8 bg-white rounded-lg shadow-md dark:bg-gray-800">
            <label class="block text-sm">
            <span class="text-gray-700 dark:text-gray-400">Nama</span>
            
            <div
                class="relative text-gray-500 focus-within:text-purple-600 dark:focus-within:text-purple-400">
                <input class="block w-full pl-10 mt-1 text-sm text-black dark:text-gray-300 dark:border-gray-600 dark:bg-gray-700 focus:border-purple-400 focus:outline-none focus:shadow-outline-purple dark:focus:shadow-outline-gray form-input" placeholder="Masukkan Nama" name="nama"/>
                @error('nama')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
                @enderror
                <div class="absolute inset-y-0 flex items-center ml-3 pointer-events-none">
                    <svg class="w-4 h-4" aria-hidden="true" fill="none" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" viewBox="0 0 24 24" stroke="currentColor" xmlns="http://www.w3.org/2000/svg">
                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M16 7a4 4 0 11-8 0 4 4 0 018 0zM12 14a7 7 0 00-7 7h14a7 7 0 00-7-7z">
                        </path>
                    </svg>
                </div>
            </div>
            </label>

            <label class="block mt-4 text-sm">
            <span class="text-gray-700 dark:text-gray-400">NIP</span>
            
            <div
                class="relative text-gray-500 focus-within:text-purple-600 dark:focus-within:text-purple-400">
                <input type="number" class="block w-full pl-10 mt-1 text-sm text-black dark:text-gray-300 dark:border-gray-600 dark:bg-gray-700 focus:border-purple-400 focus:outline-none focus:shadow-outline-purple dark:focus:shadow-outline-gray form-input" placeholder="Masukkan NIP" name="nip"/>
                @error('nip')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
                @enderror
                <div class="absolute inset-y-0 flex items-center ml-3 pointer-events-none">
                    <svg class="w-4 h-4" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M14 10l-2 1m0 0l-2-1m2 1v2.5M20 7l-2 1m2-1l-2-1m2 1v2.5M14 4l-2-1-2 1M4 7l2-1M4 7l2 1M4 7v2.5M12 21l-2-1m2 1l2-1m-2 1v-2.5M6 18l-2-1v-2.5M18 18l2-1v-2.5"></path>
                    </svg>
                </div>
            </div>
            </label>

            <label class="block mt-4 text-sm">
            <span class="text-gray-700 dark:text-gray-400">Jabatan</span>
            
            <div
                class="relative text-gray-500 focus-within:text-purple-600 dark:focus-within:text-purple-400">
                <input class="block w-full pl-10 mt-1 text-sm text-black dark:text-gray-300 dark:border-gray-600 dark:bg-gray-700 focus:border-purple-400 focus:outline-none focus:shadow-outline-purple dark:focus:shadow-outline-gray form-input" placeholder="Masukkan Jabatan" name="jabatan"/>
                @error('jabatan')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
                @enderror
                <div class="absolute inset-y-0 flex items-center ml-3 pointer-events-none">
                <svg class="w-4 h-4" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M8 7h12m0 0l-4-4m4 4l-4 4m0 6H4m0 0l4 4m-4-4l4-4"></path>
                </svg>
                </div>
            </div>
            </label>

            <label class="block mt-4 text-sm">
            <span class="text-gray-700 dark:text-gray-400">Foto</span>
            
            <div
                class="relative text-gray-500 focus-within:text-purple-600 dark:focus-within:text-purple-400">
                <input type="file" class="form-control block w-full pl-10 mt-1 text-sm text-black dark:text-gray-300 dark:border-gray-600 dark:bg-gray-700 focus:border-purple-400 focus:outline-none focus:shadow-outline-purple dark:focus:shadow-outline-gray form-input" placeholder="Edit Foto" name="gambar"/>
                @error('gambar')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
                @enderror
                <div class="absolute inset-y-0 flex items-center ml-3 pointer-events-none">
                <svg class="w-4 h-4" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M8 7h12m0 0l-4-4m4 4l-4 4m0 6H4m0 0l4 4m-4-4l4-4"></path>
                </svg>
                </div>
            </div>
            </label>
            <!-- End input form -->
        </div>
    @auth
        <div class="my-6">
            <button class="btn items-center justify-between px-4 py-2 text-sm text-white transition-colors duration-150 bg-purple-600 border border-transparent rounded-lg active:bg-purple-600 hover:bg-purple-700 focus:outline-none focus:shadow-outline-purple" type="submit">
            Tambah
                <span class="ml-2" aria-hidden="true">+</span>
            </button>
        </div>
    @endauth

    </form>
@endsection