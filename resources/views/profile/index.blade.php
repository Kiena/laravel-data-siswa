@extends('layout.index')
@section('title')
    Dashboard Siswa
@endsection

@section('content')
<div class="my-6">
    <a href="/profile/create" class="btn items-center justify-between px-4 py-2 text-sm text-white transition-colors duration-150 bg-purple-600 border border-transparent rounded-lg active:bg-purple-600 hover:bg-purple-700 focus:outline-none focus:shadow-outline-purple"
    >
    Tambah
    <span class="ml-2" aria-hidden="true">+</span>
    </a>
</div>
<div class="w-full overflow-hidden rounded-lg shadow-xs">
    <div class="w-full overflow-x-auto">
        <table class="w-full whitespace-no-wrap">
        <thead>
            <tr
            class="text-xs font-semibold tracking-wide text-left text-gray-500 uppercase border-b dark:border-gray-700 bg-gray-50 dark:text-gray-400 dark:bg-gray-800"
            >
            <th class="px-4 py-3">No</th>
            <th class="px-4 py-3">Biodata</th>
            <th class="px-4 py-3">Alamat</th>
            <th class="px-4 py-3">Telepon</th>
            </tr>
        </thead>
        <tbody class="bg-white divide-y dark:divide-gray-700 dark:bg-gray-800">
        @forelse ($profile as $key=>$value)
            <tr class="text-gray-700 dark:text-gray-400">
                <td class="px-4 py-3">
                    <div class="flex items-center text-sm">
                    <!-- Avatar with inset shadow -->
                    <div>
                        <p class="font-semibold">{{$key + 1}}</p>
                        </p>
                    </div>
                    </div>
                </td>
                <td class="px-4 py-3">
                        <div class="flex items-center text-sm">
                          <!-- Avatar with inset shadow -->
                          <div
                            class="relative hidden w-8 h-8 mr-3 rounded-full md:block"
                          >
                            <img
                              class="object-cover w-full h-full rounded-full"
                              src=""
                              alt=""
                              loading="lazy"
                            />
                            <div
                              class="absolute inset-0 rounded-full shadow-inner"
                              aria-hidden="true"
                            ></div>
                          </div>
                          <div>
                            <p class="font-semibold">{{$value->name}}</p>
                            <p class="text-xs text-gray-600 dark:text-gray-400">
                            {{$value->biodata}}
                            </p>
                          </div>
                        </div>
                      </td>
                <!-- <td class="px-4 py-3 text-sm">
                    {{$value->nama}}
                </td> -->
                <td class="px-4 py-3 text-sm">
                  
                </td>
                <td class="px-4 py-3 text-sm">
                    
                </td>
                <td class="px-4 py-3 text-sm">
                   
                </td>
                <td class="px-4 py-3 text-sm">
                    
                </td>
                <td class="px-4 py-3 text-sm">
                    
                </td>
                <td class="px-4 py-3 text-sm">
                    {{$value->created_at}}
                </td>
                <td class="px-4 py-3">
                <form action="/profile/{{$value->id}}" method="POST">
                    @csrf
                    <div class="flex items-center space-x-4 text-sm">
                    @method('DELETE')
                    <a href="/profile/{{$value->id}}"
                        class="flex items-center justify-between px-2 py-2 text-sm font-medium leading-5 text-purple-600 rounded-lg dark:text-gray-400 focus:outline-none focus:shadow-outline-gray"
                        aria-label="Delete"
                      >
                        <svg class="w-5 h-5" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M15 12a3 3 0 11-6 0 3 3 0 016 0z"></path>
                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M2.458 12C3.732 7.943 7.523 5 12 5c4.478 0 8.268 2.943 9.542 7-1.274 4.057-5.064 7-9.542 7-4.477 0-8.268-2.943-9.542-7z"></path>
                        </svg>
                    </a>
                    @auth
                      <a href="/siswa/{{$value->id}}/edit"
                        class="flex items-center justify-between px-2 py-2 text-sm font-medium leading-5 text-purple-600 rounded-lg dark:text-gray-400 focus:outline-none focus:shadow-outline-gray"
                        aria-label="Edit"
                      >
                        <svg
                          class="w-5 h-5"
                          aria-hidden="true"
                          fill="currentColor"
                          viewBox="0 0 20 20"
                        >
                          <path
                            d="M13.586 3.586a2 2 0 112.828 2.828l-.793.793-2.828-2.828.793-.793zM11.379 5.793L3 14.172V17h2.828l8.38-8.379-2.83-2.828z"
                          ></path>
                        </svg>
                        </a>
                      <button type="submit" onclick="return confirm('Apakah anda yakin?')" value="Delete"
                        class="flex items-center justify-between px-2 py-2 text-sm font-medium leading-5 text-purple-600 rounded-lg dark:text-gray-400 focus:outline-none focus:shadow-outline-gray"
                        
                      >
                        <svg
                          class="w-5 h-5"
                          aria-hidden="true"
                          fill="currentColor"
                          viewBox="0 0 20 20"
                        >
                          <path
                            fill-rule="evenodd"
                            d="M9 2a1 1 0 00-.894.553L7.382 4H4a1 1 0 000 2v10a2 2 0 002 2h8a2 2 0 002-2V6a1 1 0 100-2h-3.382l-.724-1.447A1 1 0 0011 2H9zM7 8a1 1 0 012 0v6a1 1 0 11-2 0V8zm5-1a1 1 0 00-1 1v6a1 1 0 102 0V8a1 1 0 00-1-1z"
                            clip-rule="evenodd"
                          ></path>
                        </svg>
                      </button>
                      @endauth
                    </div>
                </form>
                </td>
            </tr>
            @empty
            <tr class="text-gray-700 dark:text-gray-400">
              <td class="px-4 py-3">
                  <p class="text-sm font-semibold">No data</p>
              </td>
            </tr>  
        @endforelse
        </tbody>
        </table>
    </div>
</div>
@endsection


