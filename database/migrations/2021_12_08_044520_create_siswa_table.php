<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSiswaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('siswa', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nama', 45);
            $table->bigInteger('nisn');
            $table->integer('nilai');
            $table->text('alamat');
            $table->text('biodata');
            $table->string('jenis_kelamin', 45);
            $table->string('agama', 45);
            $table->bigInteger('telepon');
            $table->string('gambar', 100);

            $table->unsignedBigInteger('pelajaran_id');
            $table->foreign('pelajaran_id')->references('id')->on('pelajaran');

            $table->unsignedBigInteger('guru_id');
            $table->foreign('guru_id')->references('id')->on('guru');

            $table->unsignedBigInteger('kelas_id');
            $table->foreign('kelas_id')->references('id')->on('kelas');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('siswa');
    }
}
