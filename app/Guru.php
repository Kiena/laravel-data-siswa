<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Guru extends Model
{
    protected $table = 'guru';
    protected $fillable = ['nama', 'jabatan', 'nip', 'gambar'];

    public function pelajaran()
    {
        return $this->hasOne('App\Pelajaran');
    }

    public function siswa()
    {
        return $this->hasMany('App\Siswa');
    }
}
