<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
    protected $table = 'profile';
    protected $fillable = ['biodata', 'alamat', 'telepon', 'user_id'];

    public function users()
    {
        return $this->belongsTo('App\User');
    }
}
