<?php

namespace App\Http\Controllers;

use App\Pelajaran;
use App\Guru;
use DB;
use Illuminate\Http\Request;

class PelajaranController extends Controller
{
    public function index()
    {
        $pelajaran = Pelajaran::all();

        return view('pelajaran.index', compact('pelajaran'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $guru = DB::table('guru')->get();
        return view('pelajaran.create', compact('guru'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'mapel' => 'required',
            'guru_id' => 'required',
        ]);

        $pelajaran = new Pelajaran;
        $pelajaran->mapel = $request->mapel;
        $pelajaran->guru_id = $request->guru_id;

        $pelajaran->save();

        return redirect('/pelajaran');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Pelajaran  $pelajaran
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Pelajaran  $pelajaran
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $guru = DB::table('guru')->get();
        $pelajaran = Pelajaran::FindOrFail($id);
        return view('pelajaran.edit', compact('pelajaran', 'guru'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Pelajaran  $pelajaran
     * @return \Illuminate\Http\Response
     */
    public function update($id, Request $request)
    {
        $request->validate([
            'mapel' => 'required',
            'guru_id' => 'required',
        ]);

        $pelajaran = Pelajaran::find($id);
        $pelajaran->mapel = $request->mapel;
        $pelajaran->guru_id = $request->guru_id;
        $pelajaran->save();

        return redirect('/pelajaran');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Pelajaran  $pelajaran
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::table('pelajaran')->where('id', $id)->delete();

        return redirect('/pelajaran');
    }
}
