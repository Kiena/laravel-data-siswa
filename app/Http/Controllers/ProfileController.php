<?php

namespace App\Http\Controllers;

use App\Profile;
use App\User;
use Illuminate\Http\Request;

class ProfileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $profile = Profile::all();

        return view('profile.index', compact('profile'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'biodata' => 'required',
            'alamat' => 'required',
            'telepon' => 'required',
            'user_id' => 'required',
        ]);

        $profile = new Profile;
        $profile->biodata = $request->biodata;
        $profile->alamat = $request->alamat;
        $profile->telepon = $request->telepon;
        $profile->user_id = $request->user_id;

        $profile->save();

        return redirect('/profile');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Profile  $profile
     * @return \Illuminate\Http\Response
     */
    public function show(Profile $profile)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Profile  $profile
     * @return \Illuminate\Http\Response
     */
    public function edit(Profile $profile)
    {
        $users = DB::table('users')->get();
        $profile = Profile::FindOrFail($id);
        return view('profile.edit', compact('profile', 'users'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Profile  $profile
     * @return \Illuminate\Http\Response
     */
    public function update($id, Request $request)
    {
        $request->validate([
            'biodata' => 'required',
            'alamat' => 'required',
            'telepon' => 'required',
            'user_id' => 'required',
        ]);

        $profile = Profile::find($id);
        $profile->biodata = $request->biodata;
        $profile->alamat = $request->alamat;
        $profile->telepon = $request->telepon;
        $profile->user_id = $request->user_id;

        $profile->save();

        return redirect('/profile');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Profile  $profile
     * @return \Illuminate\Http\Response
     */
    public function destroy(Profile $profile)
    {
        //
    }
}
