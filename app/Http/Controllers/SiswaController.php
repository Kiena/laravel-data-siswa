<?php

namespace App\Http\Controllers;

use App\Siswa;
use App\Pelajaran;
use App\Guru;
use DB;
use Illuminate\Http\Request;

class SiswaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $siswa = Siswa::all();

        return view('siswa.index', compact('siswa'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $guru = DB::table('guru')->get();
        $kelas = DB::table('kelas')->get();
        $pelajaran = DB::table('pelajaran')->get();
        return view('siswa.create', compact('guru', 'kelas', 'pelajaran'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'nama' => 'required',
            'nisn' => 'required',
            'pelajaran_id' => 'required',
            'guru_id' => 'required',
            'kelas_id' => 'required',
            'nilai' => 'required',
            'alamat' => 'required',
            'biodata' => 'required',
            'jenis_kelamin' => 'required',
            'agama' => 'required',
            'telepon' => 'required',
            'gambar' => 'required|image|mimes:jpeg,png,jpg,gif,svg'
        ]);

        $namaGambar = time().'.'.$request->gambar->extension();  
   
        $request->gambar->move(public_path('images'), $namaGambar);

        $siswa = new Siswa;
        $siswa->nama = $request->nama;
        $siswa->nisn = $request->nisn;
        $siswa->pelajaran_id = $request->pelajaran_id;
        $siswa->guru_id = $request->guru_id;
        $siswa->kelas_id = $request->kelas_id;
        $siswa->nilai = $request->nilai;
        $siswa->alamat = $request->alamat;
        $siswa->biodata = $request->biodata;
        $siswa->jenis_kelamin = $request->jenis_kelamin;
        $siswa->agama = $request->agama;
        $siswa->telepon = $request->telepon;
        $siswa->gambar = $namaGambar;

        $siswa->save();

        return redirect('/siswa');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Siswa  $siswa
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        $siswa = Siswa::FindOrFail($id);

        return view('siswa.show', compact('siswa'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Siswa  $siswa
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $guru = DB::table('guru')->get();
        $kelas = DB::table('kelas')->get();
        $pelajaran = DB::table('pelajaran')->get();
        $siswa = Siswa::FindOrFail($id);

        return view('siswa.edit', compact('siswa', 'guru', 'kelas', 'pelajaran'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Siswa  $siswa
     * @return \Illuminate\Http\Response
     */
    public function update($id, Request $request)
    {
        $request->validate([
            'nama' => 'required',
            'nisn' => 'required',
            'pelajaran_id' => 'required',
            'guru_id' => 'required',
            'kelas_id' => 'required',
            'nilai' => 'required',
            'alamat' => 'required',
            'biodata' => 'required',
            'jenis_kelamin' => 'required',
            'agama' => 'required',
            'telepon' => 'required',
            'gambar' => 'required|image|mimes:jpeg,png,jpg,gif,svg'
        ]);

        $namaGambar = time().'.'.$request->gambar->extension();  
   
        $request->gambar->move(public_path('images'), $namaGambar);

        $siswa = Siswa::find($id);
        $siswa->nama = $request->nama;
        $siswa->nisn = $request->nisn;
        $siswa->pelajaran_id = $request->pelajaran_id;
        $siswa->guru_id = $request->guru_id;
        $siswa->kelas_id = $request->kelas_id;
        $siswa->nilai = $request->nilai;
        $siswa->alamat = $request->alamat;
        $siswa->biodata = $request->biodata;
        $siswa->jenis_kelamin = $request->jenis_kelamin;
        $siswa->agama = $request->agama;
        $siswa->telepon = $request->telepon;
        $siswa->gambar = $namaGambar;

        $siswa->save();

        return redirect('/siswa');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Siswa  $siswa
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::table('siswa')->where('id', $id)->delete();

        return redirect('/siswa');
    }
}
