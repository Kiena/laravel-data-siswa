<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Siswa extends Model
{
    protected $table = 'siswa';
    protected $fillable = ['nama', 'nisn', 'pelajaran_id', 'guru_id', 'kelas_id', 'nilai', 'alamat', 'biodata', 'jenis_kelamin', 'agama', 'telepon', 'gambar'];

    public function guru()
    {
        return $this->belongsTo('App\Guru');
    }

    public function kelas()
    {
        return $this->belongsTo('App\Kelas');
    }

    public function pelajaran()
    {
        return $this->belongsTo('App\Pelajaran');
    }
}